﻿namespace PodcastGottaCatchEmAll
{
    public interface IPodcastSettings
    {
        string FeedUrl { get; }
        string DestinationPath { get; }
        FileInfos GetFileInfos(string description, string title);
    }
}