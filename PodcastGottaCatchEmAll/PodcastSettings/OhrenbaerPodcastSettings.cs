﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;

namespace PodcastGottaCatchEmAll
{
    public class OhrenbaerPodcastSettings : IPodcastSettings
    {

        /* Structure
         * 
         * rss
         * |_ channel
         *       |_item
         *          |_title  Suse Gl&#252;ckskind (1/6): Besuch von Mrs. Fortuna
         *          |_link
         * 
         */

        public string FeedUrl => "https://www.ohrenbaer.de/podcast/podcast.feed.podcast.xml";
        public string DestinationPath => "C:\\Users\\Admin\\Downloads\\hörspiele\\Ohrenbär";

        public FileInfos GetFileInfos(string description, string title)
        {
            var prefix = "Aus der OHRENBÄR-Radiogeschichte:";
            var folgeRegex = @"(\(Folge \d von \d\))";
            var folgeMatch = Regex.Match(description, folgeRegex);
            if (!folgeMatch.Success)
            {
                throw new ArgumentException("folgenangabe nicht gefunden");
            }
            var folge = folgeMatch.Value.Trim();
            var serie = description.Substring(0, description.IndexOf("(Folge")).Substring(prefix.Length).Trim();
            var fileName = title;
            var match = Regex.Match(title, @"(\(\d/\d\))");
            if (!match.Success)
            {
                fileName = $"{serie} {folge}: {title}";
            }

            return new FileInfos
            {
                FileName = fileName.Replace("/", " von ").ReplaceForbiddenCharacters(),
                Folder = serie.ReplaceForbiddenCharacters()
            };
        }
    }
}
