﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace PodcastGottaCatchEmAll
{
    partial class Program
    {
        
        static void Main(string[] args)
        {
            var settings = new OhrenbaerPodcastSettings();

            Console.WriteLine($"reading {settings.FeedUrl}");
            XDocument feed = XDocument.Load(settings.FeedUrl);
            var items = feed.Descendants("item");
            Console.WriteLine($"found {items.Count()} items");

            var index = 1;

            foreach (var item in items)
            {
                var fullTitle = item.Descendants("title").First().Value;
                var description = item.Descendants("description").First().Value;

                Console.WriteLine($"Item {index} / {items.Count()}: {fullTitle}");
                
                var fileInfos = settings.GetFileInfos(description, fullTitle);

                var destinationFolder = Path.Combine(settings.DestinationPath, fileInfos.Folder);

                if (!Directory.Exists(destinationFolder)) Directory.CreateDirectory(destinationFolder);
                var destinationFile = $@"{destinationFolder}\\{fileInfos.FileName}.mp3";
                if (!File.Exists(destinationFile))
                {
                    using (var client = new WebClient())
                    {
                        client.DownloadFile(item.Descendants("link").First().Value, destinationFile);
                        client.Dispose();
                    }
                }
                else
                {
                    Console.WriteLine("Skipped");
                }
                Console.WriteLine("Downloaded");
                index++;

            }
            Console.WriteLine("Done - Press any key to exit");
            Console.ReadLine();
        }
    }
}


/*
 * rss
 * |_ channel
 *       |_item
 *          |_title  Suse Gl&#252;ckskind (1/6): Besuch von Mrs. Fortuna
 *          |_link
 * 
 */
