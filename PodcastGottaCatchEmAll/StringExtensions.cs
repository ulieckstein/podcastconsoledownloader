namespace PodcastGottaCatchEmAll
{
    public static class StringExtensions
    {
        public static string ReplaceForbiddenCharacters(this string input)
        {
            return input
                .Replace("?", string.Empty)
                .Replace(":", string.Empty)
                .Replace("\"", string.Empty);
        }

    }
}